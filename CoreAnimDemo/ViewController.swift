//
//  ViewController.swift
//  CoreAnimDemo
//
//  Created by James Cash on 23-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let layer1 = CALayer()
        layer1.frame = CGRect(x: 10, y: 10, width: 100, height: 100)
        layer1.backgroundColor = UIColor.red.cgColor
        view.layer.addSublayer(layer1)

        let colourAnim = CABasicAnimation(keyPath: "backgroundColor")
        colourAnim.fromValue = UIColor.red.cgColor
        colourAnim.toValue = UIColor.green.cgColor
        colourAnim.duration = 2
        colourAnim.repeatCount = Float.greatestFiniteMagnitude
        colourAnim.autoreverses = true
        layer1.add(colourAnim, forKey: "opacityAnim")

        let layer2 = CALayer()
        layer2.frame = CGRect(x: 150, y: 50, width: 100, height: 100)
        layer2.backgroundColor = UIColor.blue.cgColor
        view.layer.addSublayer(layer2)

        let yAnim = CABasicAnimation(keyPath: "position.y")
        yAnim.fromValue = 50
        yAnim.toValue = 250
        yAnim.duration = 1.5
        yAnim.repeatCount = Float.greatestFiniteMagnitude
        yAnim.autoreverses = true
        layer2.add(yAnim, forKey: nil)

        CATransaction.begin()
        CATransaction.setCompletionBlock {
            print("Animation transaction finished")
        }

        let fancyColourAnim = CAKeyframeAnimation(keyPath: "backgroundColor")
        fancyColourAnim.values = [UIColor.blue.cgColor,
                                  UIColor.red.cgColor,
                                  UIColor.green.cgColor,
                                  UIColor.black.cgColor]
        fancyColourAnim.keyTimes = [0, 0.5, 0.9, 1]
        fancyColourAnim.duration = 3.5
//        fancyColourAnim.repeatCount = Float.greatestFiniteMagnitude
        layer2.add(fancyColourAnim, forKey: "coloursAnim")

        let layer3 = CALayer()
        layer3.frame = CGRect(x: 50, y: 200, width: 100, height: 100)
        layer3.backgroundColor = UIColor.purple.cgColor
        view.layer.addSublayer(layer3)


        let pathAnim = CAKeyframeAnimation(keyPath: "position")
        pathAnim.path = CGPath(ellipseIn: view.frame, transform: nil)
        pathAnim.rotationMode = kCAAnimationRotateAuto
        pathAnim.duration = 3
//        pathAnim.repeatCount = Float.greatestFiniteMagnitude
//        pathAnim.delegate = self
        layer3.add(pathAnim, forKey: "pathAnim")

        CATransaction.commit()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        print("animation \(anim) finished")
    }
}
